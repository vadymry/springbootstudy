package com.springcrud.study.controller;

import com.springcrud.study.entity.Customer;
import com.springcrud.study.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerDAO) {
        this.customerService = customerDAO;
    }

    @GetMapping("/list")
    public String listCustomers(Model model){

        List<Customer> customerList = customerService.getCustomers();

        model.addAttribute("customers", customerList);

        return "list-customers";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model model){
        Customer customer = new Customer();

        model.addAttribute("customer", customer);
        return "customer-form";
    }

    @PostMapping(path = "/saveCustomer")
    public String saveCustomer(@ModelAttribute("customer")Customer customer){
        customerService.saveCustomer(customer);
        return "redirect:/customer/list";
    }

    @GetMapping(path = "/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("customerId")Integer id,
                                    Model model){

        Customer customer = customerService.getCustomer(id);

        model.addAttribute("customer", customer);


        return "customer-form";
    }

    @GetMapping(path = "/delete")
    public String delete(){
        return null;
    }

    @GetMapping(path = "/deleteCustomer")
    public String deleteCustomer(@RequestParam("customerId") Integer id){
        customerService.deleteCustomer(id);
        return "redirect:/customer/list";
    }
}
