package com.springcrud.study.dao;

import com.springcrud.study.entity.Customer;

import java.util.List;

public interface CustomerDAO {


    List<Customer> getCustomers();

    void saveCustomer(Customer customer);

    Customer getCustomer(Integer id);

    void deleteCustomer(Integer id);
}
