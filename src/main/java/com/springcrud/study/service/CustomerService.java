package com.springcrud.study.service;

import com.springcrud.study.entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getCustomers();

    void saveCustomer(Customer customer);

    /**
     * Retrieve Customer by id
     * @param id id of Customer
     * @return customer instance
     */
    Customer getCustomer(Integer id);

    void deleteCustomer(Integer id);
}
