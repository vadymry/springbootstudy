package com.springcrud.study.service;

import com.springcrud.study.dao.CustomerDAOImpl;
import com.springcrud.study.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{

    private final CustomerDAOImpl customerDAO;

    @Autowired
    public CustomerServiceImpl(CustomerDAOImpl customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<Customer> getCustomers() {
        return customerDAO.getCustomers();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCustomer(Customer customer) {
        customerDAO.saveCustomer(customer);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Customer getCustomer(Integer id) {
        return customerDAO.getCustomer(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteCustomer(Integer id) {
        customerDAO.deleteCustomer(id);
    }
}
